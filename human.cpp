#include "human.h"
#include "population.h"
#include <stdexcept>
#include "ccriminal.h"

Human::Human(Pasport pasp,std::string name, std::string surname,unsigned short iq, sex s, hair h,Population *popl):
    pasport(pasp),Name(name),Surname(surname),IQ(iq),Sex(s),Hair(h),population(popl)
{
    age_Y = 0;
    age_M = 0;
    Alive = true;
    Partner = nullptr;
    Profession = nullptr;
}



void Human::doActivity()
{
    grow();
    if( rand()%5<1 )
        ageDeath();//rand()%5<1 - is to decrease death chance
    if(Alive && Profession!=nullptr)
        Profession->doActivity();

}

void Human::grow()
{
    if(age_M == 11)
    {
        age_Y++;
        age_M = 0;
    }
    else
        age_M++;

    if(age_Y == 18 && age_M == 0)
    {
        population->addAdult(this);

        std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Adulthood:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") has become an adult"<<std::endl;

        float rnd = (float)rand()/RAND_MAX;
        if(rnd<0.25f){
            Profession = new cCriminal(this);
            population->addCriminal(this);
            std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Profession:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") has become a criminal"<<std::endl;
            return;
        }
        if(rnd<0.85f){
            population->addPeacefull(this);
            std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Profession:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") has become a peacefull"<<std::endl;
            return;
        }

    }
}

bool Human::ageDeath()
{
    float deathExpect = 0.7f * exp(0.004f * (age_Y*12 + age_M))/IQ;
    float rnd = (float)rand()/RAND_MAX;
    if( deathExpect < rnd  )
        return false;
    return Death();
}

bool Human::Death()
{

    if(Partner != nullptr){
        Partner->setPartner(nullptr);
        if(Partner->getSex() == Fem)//If the husband died his wife gets single (badly)
            population->addSingleFem(Partner);
        setPartner(nullptr);
    }
    Alive = false;
    population->rmHuman(pasport);
    std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Death:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") died at the age "<<age_Y<<"/"<<age_M<<std::endl;//true loger
    return true;
}

void Human::beKilled()
{
    Death();
}

void Human::setPartner(Human *partner)
{
    Partner = partner;
    if(Sex == Fem && partner!=nullptr)//Wife takes husband's surname (badly)
        Surname = partner->getSurname();
}

void Human::setProfession(profession prof)
{
    delete(Profession);
    switch(prof)
    {
        case None:  Profession = nullptr; return;
        case Criminal: Profession = new cCriminal(this); return;
        case Policeman: return;
    }
}

bool Human::isAdult()
{
    if(age_Y >= 18)
        return true;
    return false;
}

bool Human::isSingle()
{
    if(Partner == nullptr)
        return true;
    return false;
}

Pasport Human::getPasport(){return pasport;}
unsigned short Human::getIQ(){return IQ;}
std::string Human::getSurname(){return Surname;}
std::string Human::getName(){return Name;}
sex Human::getSex(){return Sex;}
bool Human::isAlive(){return Alive;}
Population* Human::getPopulation(){return population;}
info::profession Human::getProfession()
{
    if(Profession==nullptr) return None;
    return Profession->getProf();
}
