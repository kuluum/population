#include "ccriminal.h"

long unsigned cCriminal::peaceVictimAmount=0;
long unsigned cCriminal::policeVictimAmount=0;
unsigned cCriminal::largestVictimAmount=0;
Human* cCriminal::mostDangerousCriminal=nullptr;


cCriminal::cCriminal(Human *owner):Owner(owner)
{
    isPrisoner = false;
    isHiding = false;
    Stretch = 0;
    population=Owner->getPopulation();
}

void cCriminal::doActivity()
{
    if(isPrisoner){
        Stretch--;
        if(Stretch==0){
            isPrisoner=false;
            std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Release:  "<<Owner->getName()<<" "<<Owner->getSurname()<<" ("<<Owner->getPasport().ID<<") released from jail"<<std::endl;
        }
        return;
    }

    Crime();
    changeProfession();
}

void cCriminal::Crime()
{
    float rnd = (float)rand()/RAND_MAX;


    //if(rnd<0.05)
    //return;


    if(rnd<0.1)
        killPeacefull();
    else if(rnd<0.2)
        killCriminal();

    if(victimAmount>largestVictimAmount){
        largestVictimAmount=victimAmount;
        mostDangerousCriminal=Owner;
    }



}

void cCriminal::killPeacefull()
{
    Human *Aim = population->getPeacefull();
    if(Aim != Owner && Kill(Aim))
        peaceVictimAmount++;

}

void cCriminal::killCriminal()
{
    Human *Aim = population->getCriminal();
    if(Aim != Owner)
        Kill(Aim);
}

bool cCriminal::Kill(Human *Aim)
{
    if(Aim == nullptr)
        return false;

    if(Aim->getIQ()>=90)
    {
        Jailing();
        return false;
    }


    std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Murder:  "<<Owner->getName()<<" "<<Owner->getSurname()<<" ("<<Owner->getPasport().ID<<") killed ";
    std::cout<<Aim->getName()<<" "<<Aim->getSurname()<<" ("<<Aim->getPasport().ID<<")"<<std::endl;
    Aim->beKilled();
    victimAmount++;
    return true;
}

void cCriminal::Jailing()
{
    isPrisoner = true;
    Stretch = 12 * (3 + 2*victimAmount);
    if(isHiding) Stretch += 12*3;
    Population *population = Owner->getPopulation();
    std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Jailing:  "<<Owner->getName()<<" "<<Owner->getSurname()<<" ("<<Owner->getPasport().ID<<") was jailed"<<std::endl;
}

void cCriminal::Escaping()
{
    float rnd = (float)rand()/RAND_MAX;

    if(rnd<0.1){
        isHiding = true;
        isPrisoner = false;
        Stretch = 0;
        Population *population = Owner->getPopulation();
        std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Escaping:  "<<Owner->getName()<<" "<<Owner->getSurname()<<" ("<<Owner->getPasport().ID<<") escaped"<<std::endl;
    }
}

void cCriminal::changeProfession()
{
    if((float)rand()/RAND_MAX<0.2f){
        population->changeProfession(Owner, info::None);
        std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Profession:  "<<Owner->getName()<<" "<<Owner->getSurname()<<" ("<<Owner->getPasport().ID<<") changed profession from Criminal to None"<<std::endl;
    }

}

bool cCriminal::isCapable(){return !isPrisoner;}
profession cCriminal::getProf(){return Criminal;}
