#ifndef CPROFESSION_H
#define CPROFESSION_H
#include "population.h"
#include "Info.h"
class cProfession
{
public:

    virtual bool isCapable() = 0;
    virtual void doActivity() = 0;
    virtual info::profession getProf() = 0;
    virtual void changeProfession() = 0;
    virtual ~cProfession(){}
};

#endif // CPROFESSION_H
