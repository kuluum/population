
#include "cfemale.h"
#include "cmale.h"
#include "population.h"

cFemale::cFemale(Pasport pasp, std::string name, std::string surname,unsigned short iq,sex s, hair h, Population *popl):
    Human(pasp, name, surname,iq,s, h,popl)
{
    period = 0;
    childIq = 0;
    isPregnant = false;
}


void cFemale::doActivity()
{
    Human::doActivity();
    if(!isAlive() &&  (Profession!=nullptr && !Profession->isCapable()))
        return;
    if(age_Y>=18)
        Pregnancy();
}


void cFemale::Pregnancy()
{
    if(age_Y>=18 && rand()%5<2 ){//rand()%5<2  - is to decrease pregnance chance
        if(isPregnant){
            if(period==9)  Birth();
            else  period++;
        }
        else
            tryToConceive();
    }
}


void cFemale::Birth()
{
    period = 0;
    isPregnant = false;

    info::sex s = static_cast<sex>(rand()%NUM_SEX);
    info::hair h = static_cast<hair>(rand()%NUM_COLORS);

    std::string name;
    s == Male ? name = Names::getName(Male) : name = Names::getName(Fem);

    Pasport pasp;
    pasp.ID = population->getHumanCounter();

    Human *Child;
    if(s == Male) Child = new cMale(pasp, name, Surname, childIq, s, h, population);
    else Child = new cFemale(pasp, name, Surname, childIq, s, h, population);

    population->addHuman(Child);

    //std::cout<<std::endl;//true loger
    std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Childbirth:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") has bore a child at age "<<age_Y<<"/"<<age_M<<std::endl;//true loger
    std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Child:  "<<name<<" "<<Surname<<" ("<<pasp.ID<<")"<<std::endl;//true loger
    //std::cout<<std::endl;//true loger

}

void cFemale::tryToConceive()
{
    float rnd = (float)rand()/RAND_MAX;

    if( (Partner!=nullptr && rnd<0.7f) //valid pregnancy chance
            || (rnd < 0.1f)){

        Human *father;

        if( Partner!=nullptr ){
            rnd = (float)rand()/RAND_MAX;
            if( rnd<0.1f ){//treason
                father = population->getMale();
                if(father == nullptr)
                    return;
            }
            else//ok
                father = Partner;
        }
        else{//fornication
            father = population->getMale();
            if(father == nullptr)
                return;
        }

        isPregnant = true;
        childIq = (IQ + father->getIQ())/2;
    }
}


