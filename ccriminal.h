#ifndef CCRIMINAL_H
#define CCRIMINAL_H
#include "cprofession.h"
#include "human.h"
class cCriminal : public cProfession
{
public:
    cCriminal(Human* owner);
    bool isCapable();
    void doActivity();
    void changeProfession();
    ~cCriminal(){}
    profession getProf();
private:
    void Crime();
    void killPeacefull();
    void killCriminal();
    bool Kill(Human* Aim);
    void Jailing();
    void Escaping();
    Human* Owner;
    Population *population;
    bool isPrisoner;
    unsigned Stretch;
    bool isHiding;
    unsigned victimAmount;

    static long unsigned peaceVictimAmount;
    static long unsigned policeVictimAmount;
    static unsigned largestVictimAmount;
    static Human* mostDangerousCriminal;
};

#endif // CCRIMINAL_H
