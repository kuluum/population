#ifndef INFO_H
#define INFO_H
#include <string>
#include <vector>


namespace info{
enum sex
{
    Male,
    Fem,
    NUM_SEX
};

enum hair
{
    Black,
    Blond,
    Brown,
    NUM_COLORS
};

enum profession
{
    None,
    Policeman,
    Criminal
};

struct Date
{
    int Year;
    int Month;

    void incMonth()
    {
        if(Month==11){
            Year++;
            Month=0;
            return;
        }
        Month++;
    }
};

struct Pasport
{
    long int ID;
};

}
#endif // INFO_H
