#ifndef CFEMALE_H
#define CFEMALE_H

#include "human.h"

class cFemale : public Human
{
public:
    cFemale(Pasport pasp, std::string name, std::string surname,unsigned short iq,sex s, hair h, Population *popl);
    void doActivity();
    ~cFemale(){}
private:
    bool isPregnant;
    unsigned short period;
    unsigned short childIq;

    void tryToConceive();
    void Pregnancy();
    void Birth();

};

#endif // CFEMALE_H
