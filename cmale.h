#ifndef CMALE_H
#define CMALE_H
#include "human.h"
class cMale : public Human
{
public:
    cMale(Pasport pasp, std::string name, std::string surname,unsigned short iq,sex s, hair h, Population *popl);
    void doActivity();
    ~cMale(){}
private:
    void Marriage();
    void Divorce();
};

#endif // CMALE_H
