#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#include <cstdlib>
#include "Info.h"
#include "cprofession.h"
using namespace info;

class Population;

class Human
{
public:
    Human(Pasport pasp, std::string name, std::string surname,unsigned short iq,sex s, hair h, Population *popl);

    virtual void doActivity();


    void setPartner(Human *partner);
    void setProfession(info::profession prof);

    Pasport getPasport();
    unsigned short getIQ();
    std::string getSurname();
    std::string getName();
    sex getSex();
    profession getProfession();
    Population *getPopulation();

    bool isAdult();
    bool isSingle();
    bool isAlive();

    void beKilled();

    virtual ~Human(){}
protected:
    Pasport pasport;
    std::string Name;
    std::string Surname;
    bool Alive;
    unsigned short IQ;
    unsigned short age_Y;
    unsigned short age_M;
    sex Sex;
    hair Hair;

    cProfession *Profession;
    Human *Partner;
    Population *population;

private:
    void grow();
    bool ageDeath();
    bool Death();

};

#endif // PERSON_H
