#ifndef NAMES_H
#define NAMES_H
#include <vector>
#include <string>

#include "Info.h"
class Names
{
public:
    static std::string getName(info::sex sex); //return random Name from vector
    static std::string getSurname();    //return rand surname

private:
    static const std::vector<std::string> MNames;
    static const std::vector<std::string> FNames;
    static const std::vector<std::string> Surnames;

};

#endif // NAMES_H
