#include "cmale.h"
#include "population.h"

cMale::cMale(Pasport pasp, std::string name, std::string surname,unsigned short iq,sex s, hair h, Population *popl):
    Human(pasp, name, surname,iq,s, h,popl)
{
}

void cMale::doActivity()
{
    Human::doActivity();//??
    if(!isAlive() && Profession!=nullptr && !Profession->isCapable())
        return;
    if( age_Y >= 18){

        if(rand()%7<1){
            Marriage();
            return;
        }
        if(rand()%7<1) Divorce();
    }
}


void cMale::Marriage()
{
    if( (float)rand()/RAND_MAX < 0.45f && Partner==nullptr){//valid marriage chance
        Human *wife = population->getSingleFemale();
        if(wife == nullptr)
            return;

        Partner = wife;
        wife->setPartner(this);
        population->rmSingleFem(wife->getPasport());//After the wedding, wife is no longer single

        std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Wedding:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") married "<<wife->getName()<<" "<<wife->getSurname()<<" ("<<wife->getPasport().ID<<")"<<std::endl;
    }

}


void cMale::Divorce()
{
    if((float)rand()/RAND_MAX<0.25f && Partner!=nullptr)
    {
        Partner->setPartner(nullptr);
        population->addSingleFem(Partner);

        std::cout<<"["<<population->getDate().Year<<":"<<population->getDate().Month<<"]"<<"Divorce:  "<<Name<<" "<<Surname<<" ("<<pasport.ID<<") divorced "<<Partner->getName()<<" "<<Partner->getSurname()<<" ("<<Partner->getPasport().ID<<")"<<std::endl;

        Partner = nullptr;
    }
}
