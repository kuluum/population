#include "names.h"


const std::vector<std::string> Names::MNames =
{
    "John",
    "Adam",
    "Daniel"
};

const std::vector<std::string> Names::FNames =
{
        "Ann",
        "Emily",
        "Emma"
};


const std::vector<std::string> Names::Surnames =
{
    "Abramson",
    "Smith",
    "Brown",
    "Day"
};

std::string Names::getName(info::sex sex)
{

    switch (sex)
    {
    case info::Male: return MNames[ rand()%MNames.size() ];
    case info::Fem:  return FNames[ rand()%FNames.size() ];
    default:         return "Some_Name";
    }
}

std::string Names::getSurname()
{

    return Surnames[ rand()%Surnames.size() ];
}
