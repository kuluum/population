TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

#QMAKE_CXX = g++
QMAKE_CXXFLAGS += -std=c++0x



QMAKE_CXXFLAGS += -I/usr/include/mysql
#-I/usr/include/mysql++ #-DMYSQLPP_MYSQL_HEADERS_BURIED\
     #-lmysqlclient\
     #-lmysqlpp
#QMAKE_LFLAGS += -L/usr/lib -lmysqlpp -lmysqlclient -lnsl -lz -lm

QMAKE_LIBS += /usr/lib/libmysqlpp.so

SOURCES += main.cpp \
    population.cpp \
    human.cpp \
    names.cpp \
    cfemale.cpp \
    cmale.cpp \
    cprofession.cpp \
    ccriminal.cpp

HEADERS += \
    Info.h \
    population.h \
    human.h \
    names.h \
    cfemale.h \
    cmale.h \
    cprofession.h \
    ccriminal.h

