#ifndef POPULATION_H
#define POPULATION_H

#include <vector>
#include <map>

#include <string>
#include <algorithm>

#include "names.h"

class Human;
class cFemale;

class Population
{

public:
    Population();
    void rmHuman(info::Pasport pasport);
    void rmSingleFem(info::Pasport pasp);
    void rmCriminal(info::Pasport pasp);
    void rmPeacefull(info::Pasport pasp);

    void addHuman(Human* hum);
    void addAdult(Human *hum);
    void addSingleFem(Human *hum);
    void addCriminal(Human* hum);
    void addPeacefull(Human *hum);
    void changeProfession(Human* hum, info::profession prof);
    Human* getMale();
    Human* getSingleFemale();
    Human* getPeacefull();
    Human* getCriminal();
    long int getHumanCounter();

    info::Date getDate();

private:
    long int humanCounter = 0;
    info::Date date;
    void rmMale(info::Pasport pasp);

    std::vector<Human*> vPopulation;

    std::vector<Human*> vMales;
    std::vector<Human*> vSingleFemales;

    std::vector<Human*> vCriminals;
    std::vector<Human*> vPolicemans;
    std::vector<Human*> vPeacefuls;

    std::vector<Human*> vGraveyard;
};

#endif // POPULATION_H
