#include "population.h"
#include "cfemale.h"
#include "cmale.h"
#include <stdexcept>
#include "unistd.h"
Population::Population()
{
    date.Year=0;
    date.Month=0;
    humanCounter = 20;

    srand(time(0));
    for(long int i=0;i<humanCounter;i++){//filling population
        unsigned short iq = 1+rand()%100;
        info::sex s = static_cast<sex>(rand()%NUM_SEX);
        info::hair h = static_cast<hair>(rand()%NUM_COLORS);

        std::string name;
        if(s == Male)
            name = Names::getName(Male);
        else name = Names::getName(Fem);
        std::string surname = Names::getSurname();

        Pasport pasp;
        pasp.ID=i;

        Human *newHuman;
        if(s == Male) newHuman = new cMale(pasp, name, surname, iq, s, h, this);
        else newHuman = new cFemale(pasp, name, surname, iq, s, h, this);

        vPopulation.push_back(newHuman);
    }


    for(int i=0;i<12*90;i++)
    {
        for(long unsigned i =0; i<vPopulation.size(); i++)
        {
            vPopulation[i]->doActivity();
        }
        date.incMonth();
    }

    std::cout<<vPopulation.size()<<" people stil alive after modelling"<< std::endl;
}

void Population::rmHuman(Pasport pasport)
{

    for(long unsigned i=0; i<vPopulation.size(); i++)// O(n) linear search (vector is satisfies binary search)
    {
        if(vPopulation[i]->getPasport().ID == pasport.ID){
            Human *Hum = vPopulation[i];

            switch(Hum->getProfession()){
            case Criminal: rmCriminal(Hum->getPasport());break;
            case None: rmPeacefull(Hum->getPasport());break;
            case Policeman:break;
            }


            if(Hum->getSex()==info::Male && Hum->isAdult())//remove Adult Male from vMalevector
                rmMale(pasport);
            else if(Hum->getSex()==info::Fem && Hum->isAdult() && Hum->isSingle())//remove Adult Single Female from SingleFem vector
                rmSingleFem(pasport);

            vGraveyard.push_back(vPopulation[i]);
            vPopulation.erase(vPopulation.begin()+i);//remove Human from population vector

            return;
        }
    }
        throw std::runtime_error("Eraseing nonexistent human");
}


void Population::addHuman(Human *hum)
{
    if(hum->getPasport().ID == humanCounter)//just in case
    {
        vPopulation.push_back(hum);
        humanCounter++;
    }
    else
        throw std::runtime_error("Trying to add human with wrong pasport ID");
}

Human* Population::getMale()//returns random Male (or nullptr)
{
    if(vMales.size() == 0)
        return nullptr;
    return vMales[ rand()%vMales.size() ];
}

Human* Population::getSingleFemale()//returns ranom single Female (or nullptr)
{
    if(vSingleFemales.size() == 0)
        return nullptr;
    return vSingleFemales[ rand()%vSingleFemales.size() ];
}

Human* Population::getPeacefull()
{
    if(vPeacefuls.size() == 0)
        return nullptr;
    return vPeacefuls[ rand()%vPeacefuls.size() ];
}

Human* Population::getCriminal()
{
    if(vCriminals.size() == 0)
        return nullptr;
    return vCriminals[ rand()%vCriminals.size() ];
}

void Population::addAdult(Human *hum)//the name(or vectors) is so-so
{
    if(hum->getSex() == info::Male)   vMales.push_back(hum);
    else vSingleFemales.push_back(hum);
}

void Population::addSingleFem(Human *hum)
{
    if(hum->getSex()==info::Fem && hum->isSingle())//just in case
        vSingleFemales.push_back(hum);
}

void Population::addCriminal(Human *hum)
{
    if(hum->getProfession() == Criminal)
        vCriminals.push_back(hum);
}

void Population::addPeacefull(Human *hum)
{
    if(hum->getProfession() == None)
        vPeacefuls.push_back(hum);
}

void Population::rmMale(Pasport pasp)
{
    for(long unsigned i = 0; i<vMales.size(); i++){//poor search
        if(vMales[i]->getPasport().ID == pasp.ID){
            vMales.erase(vMales.begin()+i);
            return;
        }
    }
}

void Population::rmSingleFem(Pasport pasp)
{
    for(long unsigned i = 0; i<vSingleFemales.size(); i++){//poor search
        if(vSingleFemales[i]->getPasport().ID == pasp.ID){
            vSingleFemales.erase(vSingleFemales.begin()+i);
            return;
        }
    }
}

void Population::rmCriminal(info::Pasport pasp)
{
    for(long unsigned i = 0; i<vCriminals.size(); i++){//poor search
        if(vCriminals[i]->getPasport().ID == pasp.ID){
            vCriminals.erase(vCriminals.begin()+i);
            return;
        }
    }
}

void Population::rmPeacefull(info::Pasport pasp)
{
    for(long unsigned i = 0; i<vPeacefuls.size(); i++){//poor search
        if(vPeacefuls[i]->getPasport().ID == pasp.ID){
            vPeacefuls.erase(vPeacefuls.begin()+i);
            return;
        }
    }
}


void Population::changeProfession(Human *hum, info::profession prof)
{
    if(hum->getProfession()==Criminal)
    {
        rmCriminal(hum->getPasport());
        hum->setProfession(prof);
    }
}

long int Population::getHumanCounter(){return humanCounter;}
Date Population::getDate(){return date;}
